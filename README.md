Adding [AF_VSOCK](http://manpages.ubuntu.com/manpages/focal/man7/vsock.7.html) support to libxcb 1.14
=====


This fork of `libxcb` adds a new connection protocol '`vsock`' for X11 clients running in a virtual machine. Those X11 clients can connect back to the X11 server running in the host via `AF_VSOCK`.

`AF_VSOCK` can be used to provide seamless communication between virtual machines and their host with minimal overhead. TCP can also provide similar experience, but it has the inherit overhead of TCP/IP protocol and may even cause security issues. For example, when you want to forward X11 clients running in a virtual machine with an isolated network (_ex._ Hyper-V virtual machines in Windows) to its host over TCP, you must allow public access for your X11 server running in the host.

In order to protect your host from that public access, you need to set up firewall and it inevitably causes additional communication overhead as well as management headaches.

## USAGE

Although it's not well known, `libxcb` already supports various protocol names such as '_tcp_' and '_unix_'. For example, the following two `DISPLAY` variable settings are very valid and already processed exactly the same in the current version of `libxcb`:

```
export DISPLAY=127.0.0.1:0.0
export DISPLAY=tcp/127.0.0.1:0.0
```

This fork extends the existing `libxcb` protocol framework for `AF_VSOCK` by adding a new protocol name '`vsock`'. When the `DISPLAY` variable is set as the following, this fork makes connections to an X11 server via `AF_VSOCK` accordingly:
```
export DISPLAY=vsock/[<VMADDR_CID>]:<display_number>[.<screen_number>]
```
For example, in order to have X11 clients running in a virtual machine forwarded to the X11 server running in the host, you can set the `DISPLAY` variable as:

```
export DISPLAY=vsock/2:0.0
```
It can be shortened as the following:

```
export DISPLAY=vsock/:0
```

'`2`' is the assigned number for `VMADDR_CID_HOST` and it's used as a default value when it's not found in the `DISPLAY` variable. For a port number, it can be any '*unsigned int*', but in order to maintain consistency, it's defined the same as the TCP method: `6000` + _&lt;display_number&gt;_.

## TESTING

Unfortunately, I was not able to make an `AF_VSOCK` patch for the current version of [X.org server](https://www.x.org/wiki/). However, you should be able to test this fork by running a relay program such as [socat (1.7.4+)](https://stefano-garzarella.github.io/posts/2021-01-22-socat-vsock/) in your host and set the `DISPLAY` variable to '`vsock/:0`' in virtual machines after replacing their '`libxcb.so.1.1.0`' with the one from this fork:


1. Virtual Machine Host (with an X11 server running at display number '0' over a Unix socket)
   ```
   socat -b65536 VSOCK-LISTEN:6000,fork UNIX-CONNECT:/tmp/.X11-unix/X0
   ```

2. Virtual Machines (with this fork of `libxcb`)
   ```
   export DISPLAY=vsock/:0
   ```

3. Any X11 client program launched in virtual machines (2) should then be shown on the host (1).

<br />

If you're using Hyper-V or WSL2 in Windows, you can also test this fork with an X11 server for Windows that natively supports VSOCK (_ex._ [X410](https://x410.dev/cookbook/transparently-adding-native-support-for-vsock-in-x11-apps)).

<br />

<br />

<br />



About libxcb
============

libxcb provides an interface to the X Window System protocol, which
replaces the traditional Xlib interface. It has several advantages over
Xlib, including:
- size: small, simple library, and lower memory footprint
- latency hiding: batch several requests and wait for the replies later
- direct protocol access: interface and protocol correspond exactly
- proven thread support: transparently access XCB from multiple threads
- easy extension implementation: interfaces auto-generated from XML-XCB

Xlib also uses XCB as a transport layer, allowing software to make
requests and receive responses with both, which eases porting to XCB.
However, client programs, libraries, and toolkits will gain the most
benefit from a native XCB port.

More information about xcb is available from our website:

  https://xcb.freedesktop.org/

Please report any issues you find to the freedesktop.org bug tracker at:

  https://gitlab.freedesktop.org/xorg/lib/libxcb/issues

Discussion about XCB occurs on the XCB mailing list:

  https://lists.freedesktop.org/mailman/listinfo/xcb

You can obtain the latest development versions of XCB using GIT from
the libxcb code repository at:

  https://gitlab.freedesktop.org/xorg/lib/libxcb

  For anonymous checkouts, use:

    git clone https://gitlab.freedesktop.org/xorg/lib/libxcb.git

  For developers, use:

    git clone git@gitlab.freedesktop.org:xorg/lib/libxcb.git
